# BursaKerjaKhusus
1. Server Branch Final= final-server
2. Server Branch V1 = server
3. Dev Branch Final = final-dev (ongoing released branch)
4. Dev Branch V1 = develop

- Deadline = 15 April 2022

List Developer On Project
1. Reza
2. Bagus
3. Satrio

# API
- [] [Server] (https:://bursakerjasmk4.skom.id/api)

# Installation Project
- see on branch develop

# Collaboration Project
- SMKN 4 Negeri Malang

# FixingBug
- [WhatsApp] -> 089652766610

# License
- Lisensi by SMKN 4 Negeri Malang @2022


# Project Status
- Build on Laravel 8
